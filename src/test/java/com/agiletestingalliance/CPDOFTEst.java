package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class CPDOFTEst {

  @Test
  public void testMinMax() throws Exception {

    	 int result = new MinMax().getMax(2,5);
        assertEquals("getMax", 5, result);
  }

  @Test
  public void testUsefullness() throws Exception {

	 String result = new Usefulness().desc();
	 assertTrue ( result.contains("Continuous")); 
  }


  @Test
  public void TestDuration() throws Exception {

	 String result = new Duration().dur();
	 assertTrue ( result.contains("programs")); 
  }

  @Test
  public void TestAboutCPDOF() throws Exception {

	 String result = new AboutCPDOF().desc();
	 assertTrue ( result.contains("certification")); 
  }

}
