package com.agiletestingalliance;
 
public class MinMax {

    public int getMax(int intval1, int intval2) {
        if (intval2 > intval1) {
            return intval2;
        }
        else {
            return intval1;
        }
    }

}
